#include <iostream>

class Sum {

public:
    int iteration(int n) {
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum = sum + i;
        }
        return sum;
    }

public:
    int recurention(int n, int sum, int i) {
        if (i > n) {
            return sum;
        } else {
            sum = sum + i;
            recurention(n, sum, i + 1);
        }
    }

public:
    int gauss(int n) {
        return (n * (n +1)) /2;
    }

};



int main()
{
    Sum sum;
    std::cout << "iteration:" << sum.iteration(3) << "\n";
    std::cout << "recurention:" << sum.recurention(3, 0, 0) << "\n";
    std::cout << "gauss:" << sum.gauss(3) << "\n";
}

